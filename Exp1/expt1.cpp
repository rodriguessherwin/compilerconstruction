#include<iostream>
#include<string.h>
#include<ctype.h>
using namespace std;
int iskeyword(char buffer[]);
int main(){
	char ch,buffer[15];
	char str[80];
	char operators[]="+-*/%=<>!";
	char delimiter[]="[]{}();";
	int i,j=0,k=0;
	cout<<"Enter a line of code\n";
    cin.getline(str,80,'\n');
	while(str[k]!='\0'){
   		ch=str[k];
		for(i=0;i<9;i++){
   			if(ch==operators[i])
   				cout<<ch<<" Operator\n";
   		}
   		for(i=0;i<7;i++){
   			if(ch==delimiter[i])
   				cout<<ch<<" Delimiter\n";
   		}
   		if(isalnum(ch)){
   			buffer[j++]=ch;
   		}
   		else if((ch== ' ' || ch== '\n') && (j!=0)){
   				buffer[j]='\0';
   				j=0;
   				if(iskeyword(buffer) == 1)
   					cout<<buffer<<" Keyword\n";
                else if(isdigit(ch))
                    cout<<ch<<" Constant\n";
   		        else
   					cout<<buffer<<" Indentifier\n";
   		}
   		k++;
	}
	return 0;
}
int iskeyword(char buffer[]){
	char keywords[16][10] = {"int","float","long","return","switch","void","while","do","break",
                             "case","char","continue","double","if","else","for"};
	int i,flag=0;
	for(i=0;i<16;i++){
		if(strcmp(keywords[i],buffer)==0){
			flag = 1;
			break;
		}
	}
	return flag;
}
