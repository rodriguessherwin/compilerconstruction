#include<iostream>
#include<fstream>
#include<string.h>
using namespace std;

int iskeyword(char buffer[]){
	char keywords[16][10] = {"int","float","long","return","switch","void","while","do","break","case","char","continue","double","if","else","for"};
	int i,flag=0;
	for(i = 0; i < 16; ++i){
		if(strcmp(keywords[i], buffer) == 0){
			flag = 1;
			break;
		}
	}
	return flag;
}
int main(){
	char ch,buffer[15];
	char operators[] = "+-*/%=<>";
	char delimiter[]="[]{}();";
	ifstream fin("TEST_FILE.txt");
	int i,j=0;
	while(!fin.eof()){
   		ch=fin.get();
		for(i=0;i<8;i++){
   			if(ch==operators[i])
   				cout<<ch<<" Operator\n";
   		}
   		for(i=0;i<7;++i){
   			if(ch==delimiter[i])
   				cout<<ch<<" Delimiter\n";
   		}
   		if(isalnum(ch)){
   			buffer[j++]=ch;
   		}
   		else if((ch== ' ' || ch== '\n') && (j!=0)){
   				buffer[j]='\0';
   				j=0;
   				if(iskeyword(buffer) == 1)
   					cout<<buffer<<" Keyword\n";
   				else
   					cout<<buffer<<" Indentifier\n";
   		}
	}
	fin.close();
	return 0;
}
